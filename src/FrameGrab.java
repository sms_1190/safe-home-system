import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.media.Buffer;
import javax.media.CannotRealizeException;
import javax.media.CaptureDeviceInfo;
import javax.media.CaptureDeviceManager;
import javax.media.Manager;
import javax.media.NoPlayerException;
import javax.media.Player;
import javax.media.control.FrameGrabbingControl;
import javax.media.format.VideoFormat;
import javax.media.util.BufferToImage;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import java.awt.BorderLayout;

import javax.media.*;

import javax.media.protocol.*;

import javax.swing.*;

import java.awt.*;

import java.util.*;

import javax.media.control.FormatControl;

import javax.media.format.VideoFormat;

public class FrameGrab extends JPanel implements ActionListener {
	private Player player = null;

	private BufferedImage buffImg = null;

	private Timer timer;

	private FrameGrabbingControl frameGrabber;

	public FrameGrab() throws IOException, NoDataSourceException {
		// Create capture device
		Vector devices = CaptureDeviceManager.getDeviceList(null);
		CaptureDeviceInfo cdi = null;
		for (Iterator i = devices.iterator(); i.hasNext();) {
			cdi = (CaptureDeviceInfo) i.next();
			/* Get the first Video For Windows (VFW) capture device.
			 * Use the JMF registry tool in the bin directory of the JMF
			 * distribution to detect available capture devices on your
			 * computer.
			 */
			if (cdi.getName().startsWith("vfw://0"))
				break;
		}
		// start the Timer with 3s intervals
		new Timer(3000, this).start();
                MediaLocator ml = new MediaLocator("vfw://0");
                            DataSource ds = Manager.createDataSource(ml);
                            requestFormatResolution(ds);
            /* Create & start my player */

		try {
	Player player = Manager.createRealizedPlayer(ds);
			player.start();
		} catch (NoPlayerException e) {
			e.printStackTrace();
		} catch (CannotRealizeException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        try {
            Thread.sleep(10000);
        } catch (InterruptedException ex) {
            Logger.getLogger(FrameGrab.class.getName()).log(Level.SEVERE, null, ex);
        }
		frameGrabber = (FrameGrabbingControl) player.getControl(FrameGrabbingControl.class.getName());
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (buffImg != null) {
			g.drawImage(buffImg, 0, 0, this);
		}
	}

	private void grab() {
		Buffer buf = frameGrabber.grabFrame();
		// Convert frame to an buffered image so it can be processed and saved
		Image img = (new BufferToImage((VideoFormat) buf.getFormat())
				.createImage(buf));
		buffImg = new BufferedImage(img.getWidth(this), img.getHeight(this),
				BufferedImage.TYPE_INT_RGB);
		Graphics2D g = buffImg.createGraphics();
		g.drawImage(img, null, null);
		g.setColor(Color.darkGray);
		g.setFont(new Font("Tahoma", Font.PLAIN, 12)
				.deriveFont(AffineTransform.getRotateInstance(1.57)));
		g.drawString((new Date()).toString(), 5, 5);
	}

	public static void createAndShowGui() throws IOException, NoDataSourceException {
		JFrame frame = new JFrame("Frame Grabber");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(new FrameGrab());
		frame.setSize(328, 270);
		frame.setVisible(true);
	}

        public boolean requestFormatResolution(DataSource ds) {

        if (ds instanceof CaptureDevice) {

            FormatControl[] fcs = ((CaptureDevice) ds).getFormatControls();

            for (FormatControl fc : fcs) {

                Format[] formats = ((FormatControl) fc).getSupportedFormats();

                for (Format format : formats) {

                    if ((format instanceof VideoFormat)
                            && (((VideoFormat) format).getSize().getHeight() <= 480)
                            && (((VideoFormat) format).getSize().getWidth() <= 640)) {

                        ((FormatControl) fc).setFormat(format);

                        return true;

                    }

                }

            }
        }

        return false;

    }
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
                try {
                    createAndShowGui();
                } catch (IOException ex) {
                    Logger.getLogger(FrameGrab.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NoDataSourceException ex) {
                    Logger.getLogger(FrameGrab.class.getName()).log(Level.SEVERE, null, ex);
                }
			}
		});
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		grab();
		repaint();
	}
}