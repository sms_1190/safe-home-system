/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package webcam;

/**
 *
 * @author MOHIT
 */
public class DetectIntrusion {

    private Sender sender;
    private Alarm alarm;
    private Caller caller;
    SHSManager shsman;
    public DetectIntrusion(SHSManager shs) throws InterruptedException {
        shsman=shs;
       alarm=new Alarm();
       alarm.play();
       
       sender=new Sender(shsman);
       sender.send();
       Thread.sleep(2000);
       caller=new Caller(shsman);
       caller.calling();
       caller.hangup();

    }

}
