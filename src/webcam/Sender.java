/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * Class for sending message to port,checking netowrk strength and notify engine about strength,
 * receiving message from port etc.
 */
package webcam;

//import libraries
import java.util.TooManyListenersException;//for listener
import javax.comm.*;//for COM port
import java.io.*;//for input output operation
import java.sql.*;

public class Sender {

    //variable for serialport
    SerialPort serialPort;
    //command for setting port for writing purpose
    private String commands = "AT+CMGF=1\r\n";
    //variable for input stream
    private InputStream inputStream;
    //variable for output stream
    private OutputStream outputStream;
    //flag variables
    private String cell_no;
    private DatabaseConnection dc = new DatabaseConnection();
    private Connection con;

    public String getCell_no() {
        return cell_no;
    }

    public void setCell_no(String cell_no) {
        
        this.cell_no = cell_no;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    private String message;
    int result = 0;
    int lock = 0;
    String l;
    //variable of datainput stream
    DataInputStream is;
    //variable for byte buffer
    byte[] readBuffer = new byte[20];
    int flag = 0;
    //variable for CTRL + Z
    char ctrlz = (char) 26;
    //variable for printstream
    private PrintStream os = null;
    SHSManager shsman;

    public Sender(SHSManager shs) {
        shsman = shs;

        //creating instance of portintialize class
        PortInitilize p = new PortInitilize();
        try {
            //getting serialport

            serialPort = p.port_init_run();

        } catch (NoSuchPortException ex) {
            //no such port exception            
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        } catch (TooManyListenersException ex) {
            //too many listener exception
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        }

        get_cell_no_and_message();

        try {
            //creating new datainputstream
            is = new DataInputStream(serialPort.getInputStream());
            //creating new printstream
            os = new PrintStream(serialPort.getOutputStream(), true);
            //getting outputstream for serialport
            outputStream = serialPort.getOutputStream();
        } catch (IOException ex) {
            //i/p o/p exception
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        }
    }

    /*
     * Purpose : function for sending messages or any commands to serialport
     */
    public void sending(String str, int t) throws IOException {
        
        if (t == 1) {
            //if this is last line of message then write CTRL+Z after writing message
            outputStream.write(str.getBytes());
            outputStream.write(26);
            outputStream.write(13);
        } else {
            //else write only message string or command

            outputStream.write(str.getBytes());

        }
        //setting flag variable to 1
        flag = 1;
    }

    /*
     * Purpose : function for expecting some reply from serialport after sending messages or command to serialport
     */
    public int expect(String str) throws IOException {
        int r = 0;
        // System.out.println("Expected :" + str);
        String ll;
        //reading line from serialport
        ll = is.readLine();

        if (ll != null) {
            //find the index of expecting string
            //if found then set result variable to 1
            if (ll.indexOf(str) >= 0) {
                r = 1;
            } else if (ll.indexOf("ERROR") >= 0) {
                //if index of error is found then set result variable to 2
                r = 2;
            } else {
                r = 0;
            }
        }
        return r;
    }

    /*
     * Purpose : function for sending message to cell no using AT Commands
     */
    public int send() {
        shsman.set_sms_status("Sending message..");
        String mbno = getCell_no();
        String msg = getMessage();
        result = 0;
        int p = 0;

        if (lock == 0) {
            lock = 1;

            try {
                //getting output stream
                outputStream = serialPort.getOutputStream();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    javax.swing.JOptionPane.showMessageDialog(null, ex);
                }
                //sending AT+CMGF=1 command
                sending(commands, 0);

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    javax.swing.JOptionPane.showMessageDialog(null, ex);
                }
                p = 0;
                //expecting OK reply
                while ((result = expect("OK")) != 1) {
                    p = p + 1;
                    if (p > 500) {
                        result = 2;
                        break;
                    }
                    if (result == 2) {
                        break;

                    }
                }

                String mb = "AT+CMGS=\"+91" + mbno + "\"\r\n";
                //sending cell no to serialport using AT+CMGS command
                sending(mb, 0);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    javax.swing.JOptionPane.showMessageDialog(null, ex);

                }
                p = 0;
                //expecting '>' reply for writing message to serialport
                while ((result = expect(">")) != 1) {
                    p = p + 1;
                    if (p > 500) {
                        result = 2;
                        break;
                    }
                    if (result == 2) {
                        break;

                    }
                }

                //sending message to serialport
                sending(msg, 1);

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    javax.swing.JOptionPane.showMessageDialog(null, ex);

                }
                //expecting 'OK' reply
                p = 0;
                while ((result = expect("OK")) != 1) {

                    p = p + 1;
                    if (p > 500) {
                        result = 2;
                        break;
                    }
                    if (result == 2) {
                        break;

                    }
                }

            } catch (Exception ex) {
                javax.swing.JOptionPane.showMessageDialog(null, ex);
            }
            lock = 0;
        }
        shsman.set_sms_status("StandBy");
        return result;
    }

    private void get_cell_no_and_message() {
        con = dc.database_connection_main();
        try {
            Statement stmt = con.createStatement();
            String query = "select * from user_data where id=1";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                setCell_no(rs.getString("message_cell_number"));
                setMessage(rs.getString("message"));
            }
            con.close();
        } catch (SQLException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        }

    }
}
