/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webcam;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import javax.media.control.FrameGrabbingControl;

import javax.media.*;



import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.util.Date;


import javax.media.format.VideoFormat;
import javax.media.util.BufferToImage;

/**
 *
 * @author MOHIT
 */
public class ImageCapture {

    private FrameGrabbingControl frameGrabber;
    private BufferedImage buffImg;

    
    public ImageCapture() {
    }

    public void grab(int i,FrameGrabbingControl fg) {
        frameGrabber=fg;
        
        Buffer buf = frameGrabber.grabFrame();

        // Convert frame to an buffered image so it can be processed and saved

        Image img = (new BufferToImage((VideoFormat) buf.getFormat()).createImage(buf));

        buffImg = new BufferedImage(640,480, BufferedImage.TYPE_INT_RGB);

        Graphics2D g = buffImg.createGraphics();
        g.drawImage(img, null, null);

        g.setColor(Color.darkGray);
        g.setFont(new Font("Tahoma", Font.PLAIN, 12).deriveFont(AffineTransform.getRotateInstance(1.57)));
        g.drawString((new Date()).toString(), 5, 5);

        FileOutputStream out = null;
        try {
            out = new FileOutputStream("E:\\images\\" + i + ".jpg");
        } catch (java.io.FileNotFoundException io) {
            System.out.println("File Not Found");
        }

        JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
        JPEGEncodeParam param = encoder.getDefaultJPEGEncodeParam(buffImg);
        param.setQuality(0.5f, false);
        encoder.setJPEGEncodeParam(param);

        try {
            encoder.encode(buffImg);
            out.close();
        } catch (java.io.IOException io) {
            System.out.println("IOException");
        }
        //ImageIO.write(buffImg,"jpg",of);
    }
}
