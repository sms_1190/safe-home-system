/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webcam;

import java.io.*;
import javax.media.*;

/**
 *
 * @author MOHIT
 */
public class Alarm {

    private File file;
    private Player audioPlayer = null;

    public Alarm() {
        file = new File("C:\\MOHIT\\alarm.wav");
        
        try {
            audioPlayer = Manager.createRealizedPlayer(file.toURI().toURL());
        } catch (IOException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        } catch (NoPlayerException ex) {
           javax.swing.JOptionPane.showMessageDialog(null, ex);
        } catch (CannotRealizeException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        }

    }

    public void play() {
        audioPlayer.start();
    }

    public void stop() {
        audioPlayer.close();
    }
}
