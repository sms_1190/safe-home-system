/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Login.java
 *
 * Created on 15 Jun, 2010, 9:51:41 AM
 */
/*
 * Form for authentication of user.
 */
package webcam;

//import libraries
import java.sql.*;//for database

/**
 *
 * @author 
 */
public class Login extends javax.swing.JFrame {

    //creating database connection instance
    DatabaseConnection dc = new DatabaseConnection();
    //creating connection instatnce
    Connection con = dc.database_connection_main();
    //variable for engine class
   
    //variable for sender class
    

    /** Creates new form Login */
    public Login() {
        initComponents();
        //getting instances of engine and sender class
        
              
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        label2 = new java.awt.Label();
        uid = new java.awt.Label();
        user = new java.awt.TextField();
        password = new javax.swing.JPasswordField();
        login = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Login");
        setBounds(new java.awt.Rectangle(500, 130, 0, 0));

        label2.setFont(new java.awt.Font("Tahoma", 0, 12));
        label2.setForeground(new java.awt.Color(0, 0, 0));
        label2.setText("password");

        uid.setFont(new java.awt.Font("Tahoma", 0, 12));
        uid.setForeground(new java.awt.Color(0, 0, 0));
        uid.setText("user name");

        login.setText("Login");
        login.setSelected(true);
        login.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(login, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(label2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(uid, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(user, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(password, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE))))
                .addContainerGap(14, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(uid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(user, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(login, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /*
     * Purpose : function for authentication after pressing login button
     */
    private void loginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loginActionPerformed

        //getting user name
        String input = user.getText();
        //getting password
        String pass1 = password.getText();

        try {
            Statement stmt = con.createStatement();
            //query for getting users from admin table
            String query = "SELECT * FROM admin;"; // define query
            ResultSet answers = stmt.executeQuery(query);
            String p;
            while (answers.next()) {
                String name = answers.getString("user");
                String pass = answers.getString("password");

                //checking password
                if (pass1.equals(pass)) {
                    this.dispose();
                    //creating smsmanager window with passing instance of engine and sender
                    SHSManager sm = new SHSManager();
                    
                    sm.setVisible(true);
                } else {
                    //generating dialog box with error message
                    javax.swing.JOptionPane.showMessageDialog(rootPane, "Please enter valid password.");
                }

            }
        } catch (SQLException e) {
            //exception error
            javax.swing.JOptionPane.showMessageDialog(null, e);
        }

    }//GEN-LAST:event_loginActionPerformed
     public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }
    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.Label label2;
    private javax.swing.JButton login;
    private javax.swing.JPasswordField password;
    private java.awt.Label uid;
    private java.awt.TextField user;
    // End of variables declaration//GEN-END:variables
}
