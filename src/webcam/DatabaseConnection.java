/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webcam;

import java.sql.*;

/**
 *
 * @author 
 */
/**
 *
 * Class for creating database connection of any type of database
 */
public class DatabaseConnection {

    Connection con;

    /**function for creating connection with main database of application*/
    public Connection database_connection_main() {
        try {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            String url = "jdbc:odbc:Driver={Microsoft Access Driver "
                    + "(*.mdb, *.accdb)};DBQ=C:\\MOHIT\\shs.mdb";
            con = DriverManager.getConnection(url);
        } catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, e);
        } catch (ClassNotFoundException cE) {
            javax.swing.JOptionPane.showMessageDialog(null, cE);
        }
        /**returning connection*/
        return con;
    }
}
