/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package webcam;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageDecoder;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import javax.swing.Icon;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 *
 * @author MOHIT
 */
public class VerticalTextIcon implements Icon, SwingConstants{
    private Font font = UIManager.getFont("Label.font");
    private FontMetrics fm = Toolkit.getDefaultToolkit().getFontMetrics(font);

    private String text;
    private int width, height;
    private boolean clockwize;

    public VerticalTextIcon(String text, boolean clockwize){
        this.text = text;
        width = SwingUtilities.computeStringWidth(fm, text);
        width=width+20;
        
        height = fm.getHeight()-10;
        this.clockwize = clockwize;
    }

    public void paintIcon(Component c, Graphics g, int x, int y){
        Graphics2D g2 = (Graphics2D)g;
        Font oldFont = g.getFont();
        Color oldColor = g.getColor();
        AffineTransform oldTransform = g2.getTransform();

        g.setFont(font);
        g.setColor(Color.black);
        if(clockwize){
            g2.translate(x+getIconWidth(), y);
            g2.rotate(Math.PI/2);
        }else{
            g2.translate(x, y+getIconHeight());
            g2.rotate(-Math.PI/2);
        }
        //g.drawString(text, 40, fm.getLeading()+fm.getAscent());
        g.drawString(text,20,10);
        Image img=loadJPG("C:\\Users\\MOHIT\\Documents\\NetBeansProjects\\webcam\\src\\webcam\\images.jpeg");
        
        g.drawImage(img, 0, 0,20,20,null);
        g.setFont(oldFont);
        g.setColor(oldColor);
        g2.setTransform(oldTransform);
    }

    protected static Image loadJPG(String filename) {
        FileInputStream in = null;
        try {
            in = new FileInputStream(filename);
        } catch (java.io.FileNotFoundException io) {
            System.out.println("File Not Found");
        }
        JPEGImageDecoder decoder = JPEGCodec.createJPEGDecoder(in);
        BufferedImage bi = null;
        try {
            bi = decoder.decodeAsBufferedImage();
            in.close();
        } catch (java.io.IOException io) {
            System.out.println("IOException");
        }
        return bi;
    }
    public int getIconWidth(){
        return height;
    }

    public int getIconHeight(){
        return width;
    }
}
