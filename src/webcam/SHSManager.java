/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * SHSManager.java
 *
 * Created on 30 Jan, 2011, 12:47:34 PM
 */
package webcam;

import java.io.IOException;
import java.sql.*;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.media.control.FrameGrabbingControl;

import javax.media.*;

import javax.media.protocol.*;

import javax.swing.*;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.util.Date;

import javax.media.control.FormatControl;

import javax.media.format.VideoFormat;
import javax.media.util.BufferToImage;

/**
 *
 * @author MOHIT
 */
public class SHSManager extends javax.swing.JFrame {

    DatabaseConnection dc = new DatabaseConnection();
    Connection con;
    Sensor_configuration sc = new Sensor_configuration();
    private FrameGrabbingControl frameGrabber;
    private BufferedImage buffImg;
    Player p;
    DataSource ds;
    Component comp;
    ImageCapture image_capture = new ImageCapture();
    private int count;
    Camthread ct;

    /** Creates new form SHSManager */
    public SHSManager() {
        initComponents();
        SHSConfiguration shsconfig = new SHSConfiguration();
        temperature_sensor();
        motion_sensor();
        sound_sensor();
        infrared_sensor();
        shs_config_panel.add(shsconfig);
        shsconfig.setVisible(true);
        tabbed_pane.setSize(this.getWidth(), this.getHeight());
        jPanel1.setSize(this.getWidth(), this.getHeight());
        jPanel2.setSize(this.getWidth(), this.getHeight());
        jPanel3.setSize(this.getWidth(), this.getHeight());
        jPanel4.setSize(this.getWidth(), this.getHeight());
        jPanel6.setSize(this.getWidth(), this.getHeight());
        shs_config_panel.setSize(this.getWidth(), this.getHeight());

        int tabPlacement = tabbed_pane.getTabPlacement();
        switch (tabPlacement) {
            case JTabbedPane.LEFT:
            case JTabbedPane.RIGHT:
                tabbed_pane.setIconAt(0, new VerticalTextIcon("Home", tabPlacement == JTabbedPane.RIGHT));
                tabbed_pane.setIconAt(1, new VerticalTextIcon("Infrared Sensor", tabPlacement == JTabbedPane.RIGHT));
                tabbed_pane.setIconAt(2, new VerticalTextIcon("Motion Sensor", tabPlacement == JTabbedPane.RIGHT));
                tabbed_pane.setIconAt(3, new VerticalTextIcon("Temperature Sensor", tabPlacement == JTabbedPane.RIGHT));
                tabbed_pane.setIconAt(4, new VerticalTextIcon("Sound Sensor", tabPlacement == JTabbedPane.RIGHT));
                tabbed_pane.setIconAt(5, new VerticalTextIcon("SHS Configuration", tabPlacement == JTabbedPane.RIGHT));
                break;
            default:
                break;
        }

    }

    public void getCam() {

        try {

            /* Grab the default web cam*/

            MediaLocator ml = new MediaLocator("vfw://0");
            /* Create my data source */


            ds = Manager.createDataSource(ml);
            requestFormatResolution(ds);
            /* Create & start my player */

            p = Manager.createRealizedPlayer(ds);
            p.start();

            Thread.currentThread().sleep(1000);
            /* code for creating a JFrame and adding the visual component to it */

            if (p.getVisualComponent() != null) {
                comp = p.getVisualComponent();
                comp.setSize(desktoppane.getWidth(), desktoppane.getHeight());
                this.desktoppane.add(comp);
            }

            if (p.getControlPanelComponent() != null) {
                this.desktoppane.add(p.getControlPanelComponent(), BorderLayout.SOUTH);
            }
            this.pack();
            this.setVisible(true);
            frameGrabber = (FrameGrabbingControl) p.getControl(FrameGrabbingControl.class.getName());

            ImageCompare ic = new ImageCompare();
            Thread.sleep(5000);

            ct = new Camthread(frameGrabber,this);
        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    public void set_sms_status(String str)
    {
        sms_status.setText(str);
    }
    public void set_caller_status(String str)
    {
        phone_status.setText(str);
    }
    public void set_house_status(String str)
    {
        house_status.setText(str);
    }
    public boolean requestFormatResolution(DataSource ds) {

        if (ds instanceof CaptureDevice) {

            FormatControl[] fcs = ((CaptureDevice) ds).getFormatControls();

            for (FormatControl fc : fcs) {

                Format[] formats = ((FormatControl) fc).getSupportedFormats();

                for (Format format : formats) {

                    if ((format instanceof VideoFormat)
                            && (((VideoFormat) format).getSize().getHeight() <= 480)
                            && (((VideoFormat) format).getSize().getWidth() <= 640)) {

                        ((FormatControl) fc).setFormat(format);

                        return true;

                    }

                }

            }
        }

        return false;

    }

    public void temperature_sensor() {
        con = dc.database_connection_main();
        int alarm_status = 0;
        int sms_status = 0;
        int call_status = 0;
        int sensor_status = 0;
        try {
            Statement stmt = con.createStatement();
            String query = "Select * from sensor_configuration where sensor='temperature'";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                alarm_status = rs.getInt("alarm");
                sms_status = rs.getInt("sms");
                sensor_status = rs.getInt("status");
                call_status = rs.getInt("call");
            }
            if (alarm_status == 1) {
                temp_alarm_switch.setText("ON");
                temp_alarm_switch.setSelected(true);
            } else {
                temp_alarm_switch.setText("OFF");
                temp_alarm_switch.setSelected(false);
            }
            if (sensor_status == 1) {
                temp_switch.setText("ON");
                temp_switch.setSelected(true);
                temp_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/green.jpg")));
            } else {
                temp_switch.setText("OFF");
                temp_switch.setSelected(false);
                temp_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/red.jpg")));
            }
            if (call_status == 1) {
                temp_call_switch.setText("ON");
                temp_call_switch.setSelected(true);
            } else {
                temp_call_switch.setText("OFF");
                temp_call_switch.setSelected(false);
            }
            if (sms_status == 1) {
                temp_sms_switch.setText("ON");
                temp_sms_switch.setSelected(true);
            } else {
                temp_sms_switch.setText("OFF");
                temp_sms_switch.setSelected(false);
            }
            con.close();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }

    public void motion_sensor() {
        con = dc.database_connection_main();
        int alarm_status = 0;
        int sms_status = 0;
        int call_status = 0;
        int sensor_status = 0;
        try {
            Statement stmt = con.createStatement();
            String query = "Select * from sensor_configuration where sensor='motion'";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                alarm_status = rs.getInt("alarm");
                sms_status = rs.getInt("sms");
                sensor_status = rs.getInt("status");
                call_status = rs.getInt("call");
            }
            if (alarm_status == 1) {
                motion_alarm_switch.setText("ON");
                motion_alarm_switch.setSelected(true);
            } else {
                motion_alarm_switch.setText("OFF");
                motion_alarm_switch.setSelected(false);
            }
            if (sensor_status == 1) {
                motion_switch.setText("ON");
                motion_switch.setSelected(true);
                motion_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/green.jpg")));
                getCam();
            } else {
                motion_switch.setText("OFF");
                motion_switch.setSelected(false);
                motion_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/red.jpg")));
            }
            if (call_status == 1) {
                motion_call_switch.setText("ON");
                motion_call_switch.setSelected(true);
            } else {
                motion_call_switch.setText("OFF");
                motion_call_switch.setSelected(false);
            }
            if (sms_status == 1) {
                motion_sms_switch.setText("ON");
                motion_sms_switch.setSelected(true);
            } else {
                motion_sms_switch.setText("OFF");
                motion_sms_switch.setSelected(false);
            }
            con.close();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }

    public void sound_sensor() {
        con = dc.database_connection_main();
        int alarm_status = 0;
        int sms_status = 0;
        int call_status = 0;
        int sensor_status = 0;
        try {
            Statement stmt = con.createStatement();
            String query = "Select * from sensor_configuration where sensor='sound'";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                alarm_status = rs.getInt("alarm");
                sms_status = rs.getInt("sms");
                sensor_status = rs.getInt("status");
                call_status = rs.getInt("call");
            }
            if (alarm_status == 1) {
                sound_alarm_switch.setText("ON");
                sound_alarm_switch.setSelected(true);
            } else {
                sound_alarm_switch.setText("OFF");
                sound_alarm_switch.setSelected(false);
            }
            if (sensor_status == 1) {
                sound_switch.setText("ON");
                sound_switch.setSelected(true);
                sound_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/green.jpg")));
            } else {
                sound_switch.setText("OFF");
                sound_switch.setSelected(true);
                sound_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/red.jpg")));
            }
            if (call_status == 1) {
                sound_call_switch.setText("ON");
                sound_call_switch.setSelected(true);
            } else {
                sound_call_switch.setText("OFF");
                sound_call_switch.setSelected(false);
            }
            if (sms_status == 1) {
                sound_sms_switch.setText("ON");
                sound_sms_switch.setSelected(true);
            } else {
                sound_sms_switch.setText("OFF");
                sound_sms_switch.setSelected(false);
            }
            con.close();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }

    public void infrared_sensor() {
        con = dc.database_connection_main();
        int alarm_status = 0;
        int sms_status = 0;
        int call_status = 0;
        int sensor_status = 0;
        try {
            Statement stmt = con.createStatement();
            String query = "Select * from sensor_configuration where sensor='infrared'";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                alarm_status = rs.getInt("alarm");
                sms_status = rs.getInt("sms");
                sensor_status = rs.getInt("status");
                call_status = rs.getInt("call");
            }
            if (alarm_status == 1) {
                infrared_alarm_switch.setText("ON");
                infrared_alarm_switch.setSelected(true);
            } else {
                infrared_alarm_switch.setText("OFF");
                infrared_alarm_switch.setSelected(false);
            }
            if (sensor_status == 1) {
                infrared_switch.setText("ON");
                infrared_switch.setSelected(true);
                infrared_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/green.jpg")));
            } else {
                infrared_switch.setText("OFF");
                infrared_switch.setSelected(false);
                infrared_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/red.jpg")));
            }
            if (call_status == 1) {
                infrared_call_switch.setText("ON");
                infrared_call_switch.setSelected(true);
            } else {
                infrared_call_switch.setText("OFF");
                infrared_call_switch.setSelected(false);
            }
            if (sms_status == 1) {
                infrared_sms_switch.setText("ON");
                infrared_sms_switch.setSelected(true);
            } else {
                infrared_sms_switch.setText("OFF");
                infrared_sms_switch.setSelected(false);
            }
            con.close();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabbed_pane = new javax.swing.JTabbedPane();
        jPanel6 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        temp_label = new javax.swing.JLabel();
        motion_label = new javax.swing.JLabel();
        sound_label = new javax.swing.JLabel();
        infrared_label = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        house_status = new javax.swing.JTextField();
        phone_status = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        sms_status = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        infrared_switch = new javax.swing.JToggleButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        infrared_alarm_switch = new javax.swing.JToggleButton();
        jLabel3 = new javax.swing.JLabel();
        infrared_call_switch = new javax.swing.JToggleButton();
        jLabel4 = new javax.swing.JLabel();
        infrared_sms_switch = new javax.swing.JToggleButton();
        jPanel2 = new javax.swing.JPanel();
        motion_call_switch = new javax.swing.JToggleButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        motion_switch = new javax.swing.JToggleButton();
        jLabel7 = new javax.swing.JLabel();
        motion_alarm_switch = new javax.swing.JToggleButton();
        jLabel8 = new javax.swing.JLabel();
        motion_sms_switch = new javax.swing.JToggleButton();
        desktoppane = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        temp_call_switch = new javax.swing.JToggleButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        temp_switch = new javax.swing.JToggleButton();
        jLabel11 = new javax.swing.JLabel();
        temp_alarm_switch = new javax.swing.JToggleButton();
        jLabel12 = new javax.swing.JLabel();
        temp_sms_switch = new javax.swing.JToggleButton();
        jPanel4 = new javax.swing.JPanel();
        sound_call_switch = new javax.swing.JToggleButton();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        sound_switch = new javax.swing.JToggleButton();
        jLabel15 = new javax.swing.JLabel();
        sound_alarm_switch = new javax.swing.JToggleButton();
        jLabel16 = new javax.swing.JLabel();
        sound_sms_switch = new javax.swing.JToggleButton();
        shs_config_panel = new javax.swing.JPanel();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        exitMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        contentMenuItem = new javax.swing.JMenuItem();
        aboutMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(Toolkit.getDefaultToolkit().getImage("images.jpeg"));

        tabbed_pane.setTabPlacement(javax.swing.JTabbedPane.LEFT);
        tabbed_pane.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        tabbed_pane.setFont(new java.awt.Font("Tahoma", 0, 10));
        tabbed_pane.setPreferredSize(new java.awt.Dimension(200, 200));

        jLabel17.setText("Motion");

        jLabel18.setText("Temperature");

        jLabel19.setText("Sound");

        jLabel20.setText("Infrared");

        temp_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/green.jpg"))); // NOI18N

        motion_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/green.jpg"))); // NOI18N

        sound_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/green.jpg"))); // NOI18N

        infrared_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/green.jpg"))); // NOI18N

        jLabel25.setText("House Status :");

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel26.setText("Sensors :");

        house_status.setEditable(false);
        house_status.setText("Clear");

        phone_status.setEditable(false);
        phone_status.setText("StandBy");

        jLabel27.setText("Phone Status :");

        sms_status.setEditable(false);
        sms_status.setText("StandBy");

        jLabel28.setText("SMS  Status :");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel26))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel17)
                            .addComponent(motion_label))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel18)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(temp_label)
                                .addGap(9, 9, 9)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel19)
                                .addGap(5, 5, 5))
                            .addComponent(sound_label))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGap(5, 5, 5)
                                .addComponent(infrared_label))))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel25)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(house_status, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel27)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(phone_status, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel28)
                                .addGap(18, 18, 18)
                                .addComponent(sms_status, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(941, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel26)
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(infrared_label))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sound_label))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(temp_label))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(motion_label)))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(house_status, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel27)
                    .addComponent(phone_status, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28)
                    .addComponent(sms_status, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(421, Short.MAX_VALUE))
        );

        tabbed_pane.addTab("", jPanel6);

        infrared_switch.setText("OFF");
        infrared_switch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                infrared_switchActionPerformed(evt);
            }
        });

        jLabel1.setText("Sensor Switch :");

        jLabel2.setText("Alarm Switch :");

        infrared_alarm_switch.setText("OFF");
        infrared_alarm_switch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                infrared_alarm_switchActionPerformed(evt);
            }
        });

        jLabel3.setText("Auto Call :");

        infrared_call_switch.setText("OFF");
        infrared_call_switch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                infrared_call_switchActionPerformed(evt);
            }
        });

        jLabel4.setText("SMS :");

        infrared_sms_switch.setText("OFF");
        infrared_sms_switch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                infrared_sms_switchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(infrared_switch))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addGap(18, 18, 18)
                            .addComponent(infrared_call_switch))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel2)
                            .addGap(18, 18, 18)
                            .addComponent(infrared_alarm_switch))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel4)
                            .addGap(18, 18, 18)
                            .addComponent(infrared_sms_switch))))
                .addContainerGap(1091, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(infrared_switch))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(infrared_alarm_switch))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(infrared_call_switch)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(infrared_sms_switch))
                .addContainerGap(495, Short.MAX_VALUE))
        );

        tabbed_pane.addTab("", jPanel1);

        motion_call_switch.setText("OFF");
        motion_call_switch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                motion_call_switchActionPerformed(evt);
            }
        });

        jLabel5.setText("Alarm Switch :");

        jLabel6.setText("Sensor Switch :");

        motion_switch.setText("OFF");
        motion_switch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                motion_switchActionPerformed(evt);
            }
        });

        jLabel7.setText("Auto Call :");

        motion_alarm_switch.setText("OFF");
        motion_alarm_switch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                motion_alarm_switchActionPerformed(evt);
            }
        });

        jLabel8.setText("SMS :");

        motion_sms_switch.setText("OFF");
        motion_sms_switch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                motion_sms_switchActionPerformed(evt);
            }
        });

        desktoppane.setPreferredSize(new java.awt.Dimension(640, 480));

        javax.swing.GroupLayout desktoppaneLayout = new javax.swing.GroupLayout(desktoppane);
        desktoppane.setLayout(desktoppaneLayout);
        desktoppaneLayout.setHorizontalGroup(
            desktoppaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 640, Short.MAX_VALUE)
        );
        desktoppaneLayout.setVerticalGroup(
            desktoppaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 480, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(motion_switch))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(jLabel7)
                            .addGap(18, 18, 18)
                            .addComponent(motion_call_switch))
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(jLabel5)
                            .addGap(18, 18, 18)
                            .addComponent(motion_alarm_switch))
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(jLabel8)
                            .addGap(18, 18, 18)
                            .addComponent(motion_sms_switch))))
                .addGap(31, 31, 31)
                .addComponent(desktoppane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap(420, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(desktoppane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(motion_switch))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(motion_alarm_switch))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(motion_call_switch)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(motion_sms_switch))))
                .addContainerGap(140, Short.MAX_VALUE))
        );

        tabbed_pane.addTab("", jPanel2);

        temp_call_switch.setText("OFF");
        temp_call_switch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                temp_call_switchActionPerformed(evt);
            }
        });

        jLabel9.setText("Alarm Switch :");

        jLabel10.setText("Sensor Switch :");

        temp_switch.setText("OFF");
        temp_switch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                temp_switchActionPerformed(evt);
            }
        });

        jLabel11.setText("Auto Call :");

        temp_alarm_switch.setText("OFF");
        temp_alarm_switch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                temp_alarm_switchActionPerformed(evt);
            }
        });

        jLabel12.setText("SMS :");

        temp_sms_switch.setText("OFF");
        temp_sms_switch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                temp_sms_switchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(temp_switch))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addComponent(jLabel11)
                            .addGap(18, 18, 18)
                            .addComponent(temp_call_switch))
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addComponent(jLabel9)
                            .addGap(18, 18, 18)
                            .addComponent(temp_alarm_switch))
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addComponent(jLabel12)
                            .addGap(18, 18, 18)
                            .addComponent(temp_sms_switch))))
                .addContainerGap(1091, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(temp_switch))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(temp_alarm_switch))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(temp_call_switch)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(temp_sms_switch))
                .addContainerGap(495, Short.MAX_VALUE))
        );

        tabbed_pane.addTab("", jPanel3);

        sound_call_switch.setText("OFF");
        sound_call_switch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sound_call_switchActionPerformed(evt);
            }
        });

        jLabel13.setText("Alarm Switch :");

        jLabel14.setText("Sensor Switch :");

        sound_switch.setText("OFF");
        sound_switch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sound_switchActionPerformed(evt);
            }
        });
        sound_switch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                sound_switchKeyPressed(evt);
            }
        });

        jLabel15.setText("Auto Call :");

        sound_alarm_switch.setText("OFF");
        sound_alarm_switch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sound_alarm_switchActionPerformed(evt);
            }
        });

        jLabel16.setText("SMS :");

        sound_sms_switch.setText("OFF");
        sound_sms_switch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sound_sms_switchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(sound_switch))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addComponent(jLabel15)
                            .addGap(18, 18, 18)
                            .addComponent(sound_call_switch))
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addComponent(jLabel13)
                            .addGap(18, 18, 18)
                            .addComponent(sound_alarm_switch))
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addComponent(jLabel16)
                            .addGap(18, 18, 18)
                            .addComponent(sound_sms_switch))))
                .addGap(1107, 1107, 1107))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(sound_switch))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(sound_alarm_switch))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sound_call_switch)
                    .addComponent(jLabel15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(sound_sms_switch))
                .addContainerGap(495, Short.MAX_VALUE))
        );

        tabbed_pane.addTab("", jPanel4);

        javax.swing.GroupLayout shs_config_panelLayout = new javax.swing.GroupLayout(shs_config_panel);
        shs_config_panel.setLayout(shs_config_panelLayout);
        shs_config_panelLayout.setHorizontalGroup(
            shs_config_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1251, Short.MAX_VALUE)
        );
        shs_config_panelLayout.setVerticalGroup(
            shs_config_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 631, Short.MAX_VALUE)
        );

        tabbed_pane.addTab("", shs_config_panel);

        fileMenu.setText("File");

        exitMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        helpMenu.setText("Help");

        contentMenuItem.setText("Contents");
        helpMenu.add(contentMenuItem);

        aboutMenuItem.setText("About");
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tabbed_pane, javax.swing.GroupLayout.DEFAULT_SIZE, 1272, Short.MAX_VALUE)
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tabbed_pane, javax.swing.GroupLayout.DEFAULT_SIZE, 636, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void sound_switchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sound_switchActionPerformed

        AbstractButton AB = (AbstractButton) evt.getSource();
        boolean selected = AB.getModel().isSelected();
        if (selected == true) {
            sound_switch.setText("ON");
            sound_switch.setSelected(true);
            sc.update_sensor_status("sound", "status", 1);
            sound_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/green.jpg")));
        } else {
            sound_switch.setText("OFF");
            sound_switch.setSelected(false);
            sc.update_sensor_status("sound", "status", 0);
            sound_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/red.jpg")));
        }
        //if((s = evt.getActionCommand())!= null)
        //   System.out.println("Sound buttom selected = "+selected);// TODO add your handling code here:
    }//GEN-LAST:event_sound_switchActionPerformed

    private void sound_switchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_sound_switchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_sound_switchKeyPressed

    private void temp_switchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_temp_switchActionPerformed
        // TODO add your handling code here:
        AbstractButton AB = (AbstractButton) evt.getSource();
        boolean selected = AB.getModel().isSelected();
        if (selected == true) {
            temp_switch.setText("ON");
            temp_switch.setSelected(true);
            sc.update_sensor_status("temperature", "status", 1);
            temp_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/green.jpg")));
        } else {
            temp_switch.setText("OFF");
            temp_switch.setSelected(false);
            sc.update_sensor_status("temperature", "status", 0);
            temp_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/red.jpg")));
        }
    }//GEN-LAST:event_temp_switchActionPerformed

    private void infrared_switchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_infrared_switchActionPerformed
        // TODO add your handling code here:
        AbstractButton AB = (AbstractButton) evt.getSource();
        boolean selected = AB.getModel().isSelected();
        if (selected == true) {
            infrared_switch.setText("ON");
            infrared_switch.setSelected(true);
            sc.update_sensor_status("infrared", "status", 1);
            infrared_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/green.jpg")));
        } else {
            infrared_switch.setText("OFF");
            infrared_switch.setSelected(false);
            sc.update_sensor_status("infrared", "status", 0);
            infrared_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/red.jpg")));
        }
    }//GEN-LAST:event_infrared_switchActionPerformed

    private void infrared_alarm_switchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_infrared_alarm_switchActionPerformed
        // TODO add your handling code here:
        AbstractButton AB = (AbstractButton) evt.getSource();
        boolean selected = AB.getModel().isSelected();
        if (selected == true) {
            infrared_alarm_switch.setText("ON");
            infrared_alarm_switch.setSelected(true);
            sc.update_sensor_status("infrared", "alarm", 1);
        } else {
            infrared_alarm_switch.setText("OFF");
            infrared_alarm_switch.setSelected(false);
            sc.update_sensor_status("infrared", "alarm", 0);
        }
    }//GEN-LAST:event_infrared_alarm_switchActionPerformed

    private void infrared_call_switchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_infrared_call_switchActionPerformed
        // TODO add your handling code here:
        AbstractButton AB = (AbstractButton) evt.getSource();
        boolean selected = AB.getModel().isSelected();
        if (selected == true) {
            infrared_call_switch.setText("ON");
            infrared_call_switch.setSelected(true);
            sc.update_sensor_status("infrared", "call", 1);
        } else {
            infrared_call_switch.setText("OFF");
            infrared_call_switch.setSelected(false);
            sc.update_sensor_status("infrared", "call", 0);
        }
    }//GEN-LAST:event_infrared_call_switchActionPerformed

    private void infrared_sms_switchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_infrared_sms_switchActionPerformed
        // TODO add your handling code here:
        AbstractButton AB = (AbstractButton) evt.getSource();
        boolean selected = AB.getModel().isSelected();
        if (selected == true) {
            infrared_sms_switch.setText("ON");
            infrared_sms_switch.setSelected(true);
            sc.update_sensor_status("infrared", "sms", 1);
        } else {
            infrared_sms_switch.setText("OFF");
            infrared_sms_switch.setSelected(false);
            sc.update_sensor_status("infrared", "sms", 0);
        }
    }//GEN-LAST:event_infrared_sms_switchActionPerformed

    private void motion_switchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_motion_switchActionPerformed
        // TODO add your handling code here:
        AbstractButton AB = (AbstractButton) evt.getSource();
        boolean selected = AB.getModel().isSelected();
        if (selected == true) {
            motion_switch.setText("ON");
            motion_switch.setSelected(true);
            sc.update_sensor_status("motion", "status", 1);
            getCam();
            motion_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/green.jpg")));

        } else {
            motion_switch.setText("OFF");
            motion_switch.setSelected(false);
            sc.update_sensor_status("motion", "status", 0);
            try {
                stop_cam();
            } catch (IOException ex) {
                Logger.getLogger(SHSManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            motion_label.setIcon(new javax.swing.ImageIcon(getClass().getResource("/webcam/red.jpg")));
        }
    }//GEN-LAST:event_motion_switchActionPerformed

    private void motion_alarm_switchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_motion_alarm_switchActionPerformed
        // TODO add your handling code here:
        AbstractButton AB = (AbstractButton) evt.getSource();
        boolean selected = AB.getModel().isSelected();
        if (selected == true) {
            motion_alarm_switch.setText("ON");
            motion_alarm_switch.setSelected(true);
            sc.update_sensor_status("motion", "alarm", 1);
        } else {
            motion_alarm_switch.setText("OFF");
            motion_alarm_switch.setSelected(false);
            sc.update_sensor_status("motion", "alarm", 0);
        }
    }//GEN-LAST:event_motion_alarm_switchActionPerformed

    private void motion_call_switchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_motion_call_switchActionPerformed
        // TODO add your handling code here:
        AbstractButton AB = (AbstractButton) evt.getSource();
        boolean selected = AB.getModel().isSelected();
        if (selected == true) {
            motion_call_switch.setText("ON");
            motion_call_switch.setSelected(true);
            sc.update_sensor_status("motion", "call", 1);
        } else {
            motion_call_switch.setText("OFF");
            motion_call_switch.setSelected(false);
            sc.update_sensor_status("motion", "call", 0);

        }
    }//GEN-LAST:event_motion_call_switchActionPerformed

    private void motion_sms_switchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_motion_sms_switchActionPerformed
        // TODO add your handling code here:
        AbstractButton AB = (AbstractButton) evt.getSource();
        boolean selected = AB.getModel().isSelected();
        if (selected == true) {
            motion_sms_switch.setText("ON");
            motion_sms_switch.setSelected(true);
            sc.update_sensor_status("motion", "sms", 1);
        } else {
            motion_sms_switch.setText("OFF");
            motion_sms_switch.setSelected(false);
            sc.update_sensor_status("motion", "sms", 0);
        }
    }//GEN-LAST:event_motion_sms_switchActionPerformed

    private void temp_alarm_switchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_temp_alarm_switchActionPerformed
        // TODO add your handling code here:
        AbstractButton AB = (AbstractButton) evt.getSource();
        boolean selected = AB.getModel().isSelected();
        if (selected == true) {
            temp_alarm_switch.setText("ON");
            temp_alarm_switch.setSelected(true);
            sc.update_sensor_status("temperature", "alarm", 1);
        } else {
            temp_alarm_switch.setText("OFF");
            temp_alarm_switch.setSelected(false);
            sc.update_sensor_status("temperature", "alarm", 0);
        }
    }//GEN-LAST:event_temp_alarm_switchActionPerformed

    private void temp_call_switchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_temp_call_switchActionPerformed
        // TODO add your handling code here:
        AbstractButton AB = (AbstractButton) evt.getSource();
        boolean selected = AB.getModel().isSelected();
        if (selected == true) {
            temp_call_switch.setText("ON");
            temp_call_switch.setSelected(true);
            sc.update_sensor_status("temperature", "call", 1);
        } else {
            temp_call_switch.setText("OFF");
            temp_call_switch.setSelected(false);
            sc.update_sensor_status("temperature", "call", 0);
        }
    }//GEN-LAST:event_temp_call_switchActionPerformed

    private void temp_sms_switchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_temp_sms_switchActionPerformed
        // TODO add your handling code here:
        AbstractButton AB = (AbstractButton) evt.getSource();
        boolean selected = AB.getModel().isSelected();
        if (selected == true) {
            temp_sms_switch.setText("ON");
            temp_sms_switch.setSelected(true);
            sc.update_sensor_status("temperature", "sms", 1);
        } else {
            temp_sms_switch.setText("OFF");
            temp_sms_switch.setSelected(false);
            sc.update_sensor_status("temperature", "sms", 0);
        }
    }//GEN-LAST:event_temp_sms_switchActionPerformed

    private void sound_alarm_switchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sound_alarm_switchActionPerformed
        // TODO add your handling code here:
        AbstractButton AB = (AbstractButton) evt.getSource();
        boolean selected = AB.getModel().isSelected();
        if (selected == true) {
            sound_alarm_switch.setText("ON");
            sound_alarm_switch.setSelected(true);
            sc.update_sensor_status("sound", "alarm", 1);
        } else {
            sound_alarm_switch.setText("OFF");
            sound_alarm_switch.setSelected(false);
            sc.update_sensor_status("sound", "alarm", 0);
        }
    }//GEN-LAST:event_sound_alarm_switchActionPerformed

    private void sound_call_switchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sound_call_switchActionPerformed
        // TODO add your handling code here:
        AbstractButton AB = (AbstractButton) evt.getSource();
        boolean selected = AB.getModel().isSelected();
        if (selected == true) {
            sound_call_switch.setText("ON");
            sound_call_switch.setSelected(true);
            sc.update_sensor_status("sound", "call", 1);

        } else {
            sound_call_switch.setText("OFF");
            sound_call_switch.setSelected(false);
            sc.update_sensor_status("sound", "call", 0);
        }
    }//GEN-LAST:event_sound_call_switchActionPerformed

    private void sound_sms_switchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sound_sms_switchActionPerformed
        // TODO add your handling code here:
        AbstractButton AB = (AbstractButton) evt.getSource();
        boolean selected = AB.getModel().isSelected();
        
        if (selected == true) {
            sound_sms_switch.setText("ON");
            sound_sms_switch.setSelected(true);
            sc.update_sensor_status("sound", "sms", 1);
        } else {
            sound_sms_switch.setText("OFF");
            sound_sms_switch.setSelected(false);
            sc.update_sensor_status("sound", "sms", 0);
        }
    }//GEN-LAST:event_sound_sms_switchActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new SHSManager().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JMenuItem contentMenuItem;
    private javax.swing.JPanel desktoppane;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JTextField house_status;
    private javax.swing.JToggleButton infrared_alarm_switch;
    private javax.swing.JToggleButton infrared_call_switch;
    private javax.swing.JLabel infrared_label;
    private javax.swing.JToggleButton infrared_sms_switch;
    private javax.swing.JToggleButton infrared_switch;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JToggleButton motion_alarm_switch;
    private javax.swing.JToggleButton motion_call_switch;
    private javax.swing.JLabel motion_label;
    private javax.swing.JToggleButton motion_sms_switch;
    private javax.swing.JToggleButton motion_switch;
    private javax.swing.JTextField phone_status;
    private javax.swing.JPanel shs_config_panel;
    private javax.swing.JTextField sms_status;
    private javax.swing.JToggleButton sound_alarm_switch;
    private javax.swing.JToggleButton sound_call_switch;
    private javax.swing.JLabel sound_label;
    private javax.swing.JToggleButton sound_sms_switch;
    private javax.swing.JToggleButton sound_switch;
    private javax.swing.JTabbedPane tabbed_pane;
    private javax.swing.JToggleButton temp_alarm_switch;
    private javax.swing.JToggleButton temp_call_switch;
    private javax.swing.JLabel temp_label;
    private javax.swing.JToggleButton temp_sms_switch;
    private javax.swing.JToggleButton temp_switch;
    // End of variables declaration//GEN-END:variables

    private void stop_cam() throws IOException {
        ds.stop();
        p.close();
        desktoppane.remove(comp);
        desktoppane.removeAll();
    }
}
