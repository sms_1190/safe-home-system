/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webcam;

//import libraries
import java.util.TooManyListenersException;//for listener
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.comm.*;//for COM port
import java.io.*;//for input output operation
import java.sql.*;
/**
 *
 * @author MOHIT
 */
public class Caller {

    SerialPort serialPort;
    //command for setting port for writing purpose
    private String commands = "AT+CMGF=1\r\n";
    //variable for input stream
    private InputStream inputStream;
    //variable for output stream
    private OutputStream outputStream;
    private String caller_number;
    DatabaseConnection dc=new DatabaseConnection();
    Connection con;
    public String getCaller_number() {
        return caller_number;
    }

    public void setCaller_number(String caller_number) {
        this.caller_number = caller_number;
    }
    //flag variables
    int result = 0;
    int lock = 0;
    String l;
    //variable of datainput stream
    DataInputStream is;
    //variable for byte buffer
    byte[] readBuffer = new byte[20];
    int flag = 0;
    //variable for CTRL + Z
    char ctrlz = (char) 26;
    //variable for printstream
    private PrintStream os = null;

    SHSManager shsman;
    public Caller(SHSManager shs) {
        shsman=shs;
        PortInitilize p = new PortInitilize();
        try {
            //getting serialport

            serialPort = p.port_init_run();

        } catch (NoSuchPortException ex) {
            //no such port exception
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        } catch (TooManyListenersException ex) {
            //too many listener exception
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        }
        try {
            //creating new datainputstream
            is = new DataInputStream(serialPort.getInputStream());
            //creating new printstream
            os = new PrintStream(serialPort.getOutputStream(), true);
            //getting outputstream for serialport
            outputStream = serialPort.getOutputStream();
        } catch (IOException ex) {
            //i/p o/p exception
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        }
        get_caller_number();
    }

    public void sending(String str) throws IOException {
        //else write only message string or command
        outputStream.write(str.getBytes());
        //setting flag variable to 1
        flag = 1;
    }

    public int expect(String str) throws IOException {
        int r = 0;
        // System.out.println("Expected :" + str);
        String ll;
        //reading line from serialport
        ll = is.readLine();

        if (ll != null) {
            //find the index of expecting string
            //if found then set result variable to 1
            if (ll.indexOf(str) >= 0) {
                r = 1;
            } else if (ll.indexOf("ERROR") >= 0) {
                //if index of error is found then set result variable to 2
                r = 2;
            } else {
                r = 0;
            }
        }
        return r;
    }

    public void calling() {
        try {
            shsman.set_caller_status("calling..");
            String caller="ATD "+getCaller_number()+";\r\n";
            sending(caller);
            int pp = 0;
            while ((result = expect("OK")) != 1) {
                pp = pp + 1;
                if (pp > 500) {
                    result = 2;
                    break;
                }
                if (result == 2) {
                    break;

                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Sender.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            Thread.sleep(4000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Caller.class.getName()).log(Level.SEVERE, null, ex);
        }
            shsman.set_caller_status("StandBy");
    }

    public void hangup() {
        try {
            sending("ATH\r\n");
            int pp = 0;
            while ((result = expect("OK")) != 1) {
                pp = pp + 1;
                if (pp > 500) {
                    result = 2;
                    break;
                }
                if (result == 2) {
                    break;

                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Sender.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void get_caller_number() {
        con = dc.database_connection_main();
        try {
            Statement stmt = con.createStatement();
            String query = "select * from user_data where id=1";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                setCaller_number(rs.getString("caller_cell_number"));
                
            }
            con.close();
        } catch (SQLException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        }

    }
}
