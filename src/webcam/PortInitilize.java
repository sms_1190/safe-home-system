/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webcam;

import java.sql.*;
import java.util.TooManyListenersException;
import javax.comm.*;
import java.io.*;

/**
 *
 * @author 
 */
/**
 * Class for initializing port and getting input and output stream
 *
 */
public class PortInitilize {

    /**instance of CommPortIdentifier class*/
    static CommPortIdentifier portId;
    /**instance of SerialPort Class*/
    static SerialPort serialPort;
    /**instance for output and input stream class*/
    private OutputStream os;
    private InputStream is;
    /**instance for DatabaseConnection Class*/
    DatabaseConnection dc = new DatabaseConnection();
    Connection con = dc.database_connection_main();

    /**Function for initializing port and returning  SerialPort instance*/
    public SerialPort port_init_run() throws NoSuchPortException, TooManyListenersException {

        String port = null;
        /**Initializing default parameters for Serial Port*/
        int baud_rate = 2400;
        int data_bits = 8;
        int stop_bit = 1;
        int read_timeout;
        int write_timeout;
        int parity_bit = 0;

        try {

            Statement stmt = con.createStatement();
            /**Retrieving port and its parameter from database*/
            String query = "SELECT * FROM port_config;"; // define query
            ResultSet answers = stmt.executeQuery(query);

            while (answers.next()) {
                port = answers.getString("port_name");
                baud_rate = Integer.parseInt(answers.getString("baud_rate"));
                data_bits = Integer.parseInt(answers.getString("data_bits"));
                stop_bit = Integer.parseInt(answers.getString("stop_bit"));
                parity_bit = Integer.parseInt(answers.getString("parity_bit"));
                read_timeout = Integer.parseInt(answers.getString("read_timeout"));
                write_timeout = Integer.parseInt(answers.getString("write_timeout"));

            }
            portId = (CommPortIdentifier) CommPortIdentifier.getPortIdentifier(port);

            if (portId.getName().equals(port)) //your comm port
            {
                //system.out.println("Com port found.");
                try {
                    /**Opening serial port for communication*/
                    serialPort = (SerialPort) portId.open("SMSSENDER", 2000);
                    try {
                        /**setting the parameters of serial port*/
                        serialPort.setSerialPortParams(baud_rate, data_bits, stop_bit, parity_bit);

                    } catch (UnsupportedCommOperationException ex) {
                        javax.swing.JOptionPane.showMessageDialog(null, ex);
                    }
                    /*try {
                    os = serialPort.getOutputStream();
                    is = serialPort.getInputStream();
                    } catch (IOException e) {
                    serialPort.close();

                    }*/

                    try {
                        serialPort.enableReceiveTimeout(30);
                    } catch (UnsupportedCommOperationException e) {
                    }

                } catch (PortInUseException e) {
                    //system.out.println(e);
                }
            }
            con.close();

        } catch (SQLException e) {
            //system.out.println("SQL Exception: " + e.toString());
        }
        return serialPort;
    }
}
