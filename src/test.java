
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageDecoder;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.media.Buffer;
import javax.media.CannotRealizeException;
import javax.media.CaptureDeviceInfo;
import javax.media.CaptureDeviceManager;
import javax.media.Manager;
import javax.media.NoPlayerException;
import javax.media.Player;
import javax.media.control.FrameGrabbingControl;
import javax.media.format.VideoFormat;
import javax.media.util.BufferToImage;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.imageio.*;
import java.awt.BorderLayout;

import javax.media.*;

import javax.media.protocol.*;

import javax.swing.*;

import java.awt.*;
import java.awt.image.Raster;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.util.*;

import javax.media.control.FormatControl;

import javax.media.format.VideoFormat;

public class test {

    JFrame jfrm = new JFrame("Testing Webcam");

    public static void main(String[] args) {

        test t = new test();

        t.getCam();

    }
    private FrameGrabbingControl frameGrabber;
    private BufferedImage buffImg;
    private BufferedImage img1;
    private BufferedImage imgc;
    private int factorD;
    private boolean match;
    private int debugMode;
    private BufferedImage img2;
    private int comparex;
    private int comparey;
    private int factorA;

    public void getCam() {

        try {

            /* Grab the default web cam*/

            MediaLocator ml = new MediaLocator("vfw://0");
            /* Create my data source */

            DataSource ds = Manager.createDataSource(ml);
            requestFormatResolution(ds);
            /* Create & start my player */

            Player p = Manager.createRealizedPlayer(ds);
            p.start();
            Thread.currentThread().sleep(1000);
            /* code for creating a JFrame and adding the visual component to it */



            jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            if (p.getVisualComponent() != null) {
                jfrm.getContentPane().add(p.getVisualComponent());
            }

            if (p.getControlPanelComponent() != null) {
                jfrm.getContentPane().add(p.getControlPanelComponent(), BorderLayout.SOUTH);
            }

            jfrm.pack();

            jfrm.setLocationRelativeTo(null);

            jfrm.setVisible(true);

            jfrm.setSize(640, 480);
            frameGrabber = (FrameGrabbingControl) p.getControl(FrameGrabbingControl.class.getName());
            int i = 1;
            while (true) {
                Thread.sleep(5000);
                grab(i);
                if (i > 3) {
                    break;
                }
                i = i + 1;
            }
            ImageCompare("E:\\1.jpg", "E:\\2.jpg");

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    public void ImageCompare(String file1, String file2) {
        ImageCompare2(loadJPG(file1), loadJPG(file2));
    }

    public void ImageCompare2(Image img1, Image img2) {
        ImageCompare1(imageToBufferedImage(img1), imageToBufferedImage(img2));
    }

    public BufferedImage getChangeIndicator() {
        return imgc;
    }

    protected int getAverageBrightness(BufferedImage img) {
        Raster r = img.getData();
        int total = 0;
        for (int y = 0; y < r.getHeight(); y++) {
            for (int x = 0; x < r.getWidth(); x++) {
                total += r.getSample(r.getMinX() + x, r.getMinY() + y, 0);
            }
        }
        return (int) (total / ((r.getWidth() / factorD) * (r.getHeight() / factorD)));
    }

    protected static Image loadJPG(String filename) {
        FileInputStream in = null;
        try {
            in = new FileInputStream(filename);
        } catch (java.io.FileNotFoundException io) {
            System.out.println("File Not Found");
        }
        JPEGImageDecoder decoder = JPEGCodec.createJPEGDecoder(in);
        BufferedImage bi = null;
        try {
            bi = decoder.decodeAsBufferedImage();
            in.close();
        } catch (java.io.IOException io) {
            System.out.println("IOException");
        }
        return bi;
    }
    // returns true if image pair is considered a match

    public boolean match() {
        return this.match;
    }

    // buffered images are just better.
/*protected	*/
    public static BufferedImage imageToBufferedImage(Image img) {
        BufferedImage bi = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = bi.createGraphics();
        g2.drawImage(img, null, null);
        return bi;
    }

    public void ImageCompare1(BufferedImage img1, BufferedImage img2) {
        this.img1 = img1;
        this.img2 = img2;
        autoSetParameters();
    }

    // like this to perhaps be upgraded to something more heuristic in the future.
    protected void autoSetParameters() {
        comparex = 10;
        comparey = 10;
        factorA = 10;
        factorD = 10;
        compare();
    }

    public void compare() {
        // setup change display image

        imgc = imageToBufferedImage(img2);
        Graphics2D gc = imgc.createGraphics();
        gc.setColor(Color.RED);
        // convert to gray images.
        img1 = imageToBufferedImage(GrayFilter.createDisabledImage(img1));
        img2 = imageToBufferedImage(GrayFilter.createDisabledImage(img2));
        // how big are each section
        int columns = img1.getWidth();
        int rows = img2.getHeight();

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < columns; col++) {
                int rgb = img1.getRGB(col, row);
                int rgb2 = img2.getRGB(col, row);
                if (rgb == rgb2) {
                    System.out.println("same");
                } else {
                    System.out.println("different");
                }
            }
        }
        /*
        int blocksx = (int) (img1.getWidth() / comparex);
        int blocksy = (int) (img1.getHeight() / comparey);
        // set to a match by default, if a change is found then flag non-match
        this.match = true;
        // loop through whole image and compare individual blocks of images
        for (int y = 0; y < comparey; y++) {
        if (debugMode > 0) {
        System.out.print("|");
        }
        for (int x = 0; x < comparex; x++) {
        int b1 = getAverageBrightness(img1.getSubimage(x * blocksx, y * blocksy, blocksx - 1, blocksy - 1));
        int b2 = getAverageBrightness(img2.getSubimage(x * blocksx, y * blocksy, blocksx - 1, blocksy - 1));
        int diff = Math.abs(b1 - b2);
        if (diff > factorA) { // the difference in a certain region has passed the threshold value of factorA
        // draw an indicator on the change image to show where change was detected.
        gc.drawRect(x * blocksx, y * blocksy, blocksx - 1, blocksy - 1);
        this.match = false;
        }
        if (debugMode == 1) {
        System.out.print((diff > factorA ? "X" : " "));
        }
        if (debugMode == 2) {
        System.out.print(diff + (x < comparex - 1 ? "," : ""));
        }
        }
        if (debugMode > 0) {
        System.out.println("|");
        }
        }*/
    }

    private void grab(int i) {

        System.out.println(frameGrabber);
        Buffer buf = frameGrabber.grabFrame();

        // Convert frame to an buffered image so it can be processed and saved

        Image img = (new BufferToImage((VideoFormat) buf.getFormat()).createImage(buf));

        buffImg = new BufferedImage(img.getWidth(jfrm), img.getHeight(jfrm), BufferedImage.TYPE_INT_RGB);

        Graphics2D g = buffImg.createGraphics();
        g.drawImage(img, null, null);

        g.setColor(Color.darkGray);
        g.setFont(new Font("Tahoma", Font.PLAIN, 12).deriveFont(AffineTransform.getRotateInstance(1.57)));
        g.drawString((new Date()).toString(), 5, 5);

        FileOutputStream out = null;
        try {
            out = new FileOutputStream("E:\\" + i + ".jpg");
        } catch (java.io.FileNotFoundException io) {
            System.out.println("File Not Found");
        }

        JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
        JPEGEncodeParam param = encoder.getDefaultJPEGEncodeParam(buffImg);
        param.setQuality(0.5f, false);
        encoder.setJPEGEncodeParam(param);

        try {
            encoder.encode(buffImg);
            out.close();
        } catch (java.io.IOException io) {
            System.out.println("IOException");
        }
        //ImageIO.write(buffImg,"jpg",of);
    }

    public boolean requestFormatResolution(DataSource ds) {

        if (ds instanceof CaptureDevice) {

            FormatControl[] fcs = ((CaptureDevice) ds).getFormatControls();

            for (FormatControl fc : fcs) {

                Format[] formats = ((FormatControl) fc).getSupportedFormats();

                for (Format format : formats) {

                    if ((format instanceof VideoFormat)
                            && (((VideoFormat) format).getSize().getHeight() <= 480)
                            && (((VideoFormat) format).getSize().getWidth() <= 640)) {

                        ((FormatControl) fc).setFormat(format);

                        return true;

                    }

                }

            }
        }

        return false;

    }
}
